#include <sourcemod>
#include <socket>
#include <confogl>
#include <json>

#define DEBUG 1

#define API_QUERY_INTERVAL 10.0
#define API_REPORT_INTERVAL 10.0

#define API_HOST "xladder.user.skyvps.ru"
#define API_URI_GET_MATCHES "GET /api/matches/any?port="
#define API_URI_REPORT_STATUS "PATCH /api/matches/"

#define OAUTH_KEY_PATH "configs/xladder.txt"

new String:sOAuthToken[2048];
new String:map[64];
new String:config[64];
		
new Handle:g_hAuthFile;
new Handle:g_hCvarMatchId;

new bool:bMatchStarting;

public Plugin:myinfo = 
{
	name = "L4D2 X-Ladder Server Plugin",
	author = "",
	description = "",
	version = "0.0.1.7"
};

public OnPluginStart()
{
	decl String:sBuffer[128];
	GetGameFolderName(sBuffer, sizeof(sBuffer));
	if (!StrEqual(sBuffer, "left4dead2", false))
	{
		SetFailState("Plugin supports Left 4 dead 2 only!");
	}
	
	g_hCvarMatchId = CreateConVar("xladder_match_id", "0", "Current XLadder match ID; transient cvar -- DO NOT CHANGE", FCVAR_PLUGIN);
	
	HookEvent("round_start", EventHook:OnRoundStart, EventHookMode_PostNoCopy);
	
	// Get auth token
	g_hAuthFile = CreateKeyValues("XLadder");
	BuildPath(Path_SM, sBuffer, sizeof(sBuffer), OAUTH_KEY_PATH);
	if (!FileToKeyValues(g_hAuthFile, sBuffer))
	{
		SetFailState("Couldn't load the OAuth key file!");
	}
	if (!KvJumpToKey(g_hAuthFile, "OAuthToken"))
	{
		SetFailState("Couldn't find the OAuthToken entry!");
	}
	KvGetString(g_hAuthFile, "OAuthToken", sOAuthToken, sizeof(sOAuthToken));
	CloseHandle(g_hAuthFile);
}

public OnMapStart()
{
	// Launch the config if we've just loaded the target map
	if (bMatchStarting)
	{
		decl String:sCommandBuffer[256];
		Format(sCommandBuffer, sizeof(sCommandBuffer), "sm_forcematch %s", config);
		ServerCommand(sCommandBuffer);
	}
}

public OnRoundStart()
{
	// Confogl is running, do not query the ladder site, report instead
	if (LGO_IsMatchModeLoaded())
	{
		new matchId = GetConVarInt(g_hCvarMatchId);
		
		// Check if it's a ladder game
		if (matchId < 1)
		{
			return;
		}
		
		CreateTimer(API_REPORT_INTERVAL, ReportMatchStatus, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
	else
	{
		CreateTimer(API_QUERY_INTERVAL, QueryPendingMatches, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action:QueryPendingMatches(Handle:timer)
{
	if (bMatchStarting)
		return;
	
	new Handle:socket = SocketCreate(SOCKET_TCP, OnSocketError);
	
	SocketConnect(socket, OnSocketConnected, OnSocketReceive, OnSocketDisconnected, API_HOST, 80);
}

public Action:ReportMatchStatus(Handle:timer)
{
	if (bMatchStarting)
		return;
	
	new Handle:socket = SocketCreate(SOCKET_TCP, OnSocketError);
	
	SocketConnect(socket, OnSocketConnected, OnSocketReceive, OnSocketDisconnected, API_HOST, 80);
}

public OnSocketConnected(Handle:socket, any:arg) 
{
#if DEBUG
	PrintToChatAll("\x01Socket successfully connected, continuing...");
#endif

	decl String:sRequestStr[3072];
	Format(sRequestStr, sizeof(sRequestStr), "%s%d HTTP/1.0\r\nHost: %s\r\nAuthorization: Bearer %s\r\nConnection: close\r\n\r\n", API_URI_GET_MATCHES, GetConVarInt(FindConVar("hostport")), API_HOST, sOAuthToken);
	SocketSend(socket, sRequestStr);
}

public OnSocketReceive(Handle:socket, String:receiveData[], const dataSize, any:junkvar) 
{
#if DEBUG
	PrintToChatAll("\x01Received data: \x04%s\x01 (size \x05%d\x01)", receiveData, dataSize);
#endif
	// Strip headers, get to the data
	decl String:buffer[2][1024];
	ExplodeString(receiveData, "\r\n\r\n", buffer, 2, 1024);

	// TODO: does this mean there are no pending matches? check for 200 OK
	if (buffer[1][0] == EOS)
	{
		LogMessage("[XLADDER] Received empty reply from the server!");
	#if DEBUG
		PrintToChatAll("[XLADDER] Received empty reply from the server!");
	#endif
		return;
	}

	new JSON:json = json_decode(buffer[1]); 
	if (json == JSON_INVALID)
	{ 
		LogMessage("[XLADDER] Failed to decode JSON data!");
	#if DEBUG
		PrintToChatAll("[XLADDER] Failed to decode JSON data!");
	#endif
	}
	else
	{
		new matchId;
		new playerCount;
		json_get_cell(json, "match_id", matchId);
		json_get_cell(json, "match_users_need", playerCount);
		
		// Save this match ID across config reloads
		SetConVarInt(g_hCvarMatchId, matchId);
		
		// todo: compare if destination map is already on, if yes skip setting it and launch the config straight away, otherwise set a bool and catch in the next onmapstart
		json_get_string(json, "map", map, sizeof(map));
		json_get_string(json, "config", config, sizeof(config));
		
        

		// Switch map
		bMatchStarting = true;
		
		decl String:sCommandBuffer[256];
		Format(sCommandBuffer, sizeof(sCommandBuffer), "changelevel %s", map);
		ServerCommand(sCommandBuffer);
	} 
}

public OnSocketDisconnected(Handle:socket, any:map) 
{
	CloseHandle(socket);
}

public OnSocketError(Handle:socket, const errorType, const errorNum, any:map) 
{
#if DEBUG
	PrintToChatAll("\x01Socket error \x05%d\x01 (errno \x05%d\x01)", errorType, errorNum);
#endif
	LogError("socket error %d (errno %d)", errorType, errorNum);
	CloseHandle(socket);
}